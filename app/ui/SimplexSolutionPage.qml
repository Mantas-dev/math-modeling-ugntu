import QtQuick 2.12
import QtQml 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import "."

Item {
    id      : container
    width   : 1200
    height  : 900

    QtObject {
        id : qtObj
        property var componentsArray: [];
    }

    Flickable {
        id                  : containerFlickable
        contentHeight       : groupItem.height
        boundsBehavior      : Flickable.StopAtBounds
        ScrollBar.vertical  : ScrollBar { policy: ScrollBar.AlwaysOn }
        anchors.fill        : parent

        Item {
            id      : groupItem
            width   : parent.width
            height  : childrenRect.height + 30

            Text {
                id                          : commonHeaderText
                text                        : "Решение задачи линейного программирования симплексным методом"
                width                       : parent.width
                padding                     : 20
                wrapMode                    : Text.Wrap
                horizontalAlignment         : Text.AlignHCenter
                anchors.horizontalCenter    : parent.horizontalCenter
                font {
                    pixelSize   : 25
                    family      : AppTheme.fontProximaNovaBold.name
                    bold        : true
                }
            }

            Text {
                id                  : tableHeaderText
                text                : "Заполните таблицу:"
                leftPadding         : 20
                wrapMode            : Text.Wrap
                anchors {
                    top         : commonHeaderText.bottom
                    leftMargin  : 20
                }
                font {
                    pixelSize   : 18
                    family      : AppTheme.fontProximaNovaBold.name
                    bold        : true
                }
            }

            CustomTable {
                id          : initialTable
                tableModel  : ModelManager.simplexTableModel
                anchors {
                    top         : tableHeaderText.bottom
                    left        : parent.left
                    topMargin   : 20
                    leftMargin  : 20
                }
            }

            Text {
                id          : objectiveDescriptionText
                text        : "Определить максимальную прибыль от реализации всей продукции видов X, Y и Z. Решить задачу симплекс-методом."
                width       : parent.width
                leftPadding : 20
                wrapMode    : Text.Wrap
                anchors {
                    top         : initialTable.bottom
                    topMargin   : 30
                }
                font {
                    pixelSize   : 18
                    family      : AppTheme.fontProximaNovaBold.name
                    bold        : true
                }
            }

            CustomMainButton {
                id              : backButton
                buttonText      : "Назад"
                buttonHeight    : 70
                buttonWidth     : buttonText.length * 30
                buttonColor     : AppTheme.backButtonColor
                buttonClickColor: AppTheme.backButtonClickColor
                buttonHoverColor: AppTheme.backButtonHoverColor
                anchors {
                    top         : objectiveDescriptionText.bottom
                    left        : parent.left
                    topMargin   : 30
                    leftMargin  : 20
                }
                onClicked: {
                    Init.requestPopPage();
                }
            }

            CustomMainButton {
                id              : startCalculateButton
                buttonText      : "Решить симплексным методом"
                buttonHeight    : 70
                buttonWidth     : buttonText.length * 15
                anchors {
                    top         : objectiveDescriptionText.bottom
                    left        : backButton.right
                    topMargin   : 30
                    leftMargin  : 20
                }
                onClicked: {
                    if (qtObj.componentsArray.length > 0)
                        destroyAllComponents();
                    ModelManager.resolveBySimplex();
                }
            }

            Text {
                id          : solutionHeader
                text        : "Решение:"
                wrapMode    : Text.Wrap
                visible     : colContainer.childrenRect.height != 0
                anchors {
                    top         : startCalculateButton.bottom
                    left        : parent.left
                    topMargin   : 20
                    leftMargin  : 20
                }
                font {
                    pixelSize   : 25
                    family      : AppTheme.fontProximaNovaBold.name
                    bold        : true
                }
            }

            Column {
                id      : colContainer
                spacing : 50
                visible : colContainer.childrenRect.height != 0
                anchors {
                    top     : solutionHeader.bottom
                    left    : parent.left
                    right   : parent.right
                    margins : 20
                }
            }
        }
    }

    function destroyAllComponents(){
        for (var i = 0; i < qtObj.componentsArray.length; i++) {
            var currentComponent = qtObj.componentsArray[i];
            currentComponent.destroy();
        }
        qtObj.componentsArray = [];
    }

    Connections {
        target: ModelManager
        onTableModelCreated: {
            var tableModelComponent = Qt.createComponent("qrc:/app/ui/CustomTableWithHeader.qml");
            var tableModelobject = tableModelComponent.createObject(colContainer);
            tableModelobject.headerText = title;
            tableModelobject.tableModel = model;
            qtObj.componentsArray.push(tableModelobject);
        }

        onSendText: {
            var textObject = Qt.createQmlObject("import QtQuick 2.0; Text {}", colContainer);
            textObject.font.pixelSize = 18;
            textObject.font.family = AppTheme.fontProximaNovaBold.name;
            textObject.textFormat = Text.RichText;
            textObject.text = text;
            qtObj.componentsArray.push(textObject);
        }
    }
}
