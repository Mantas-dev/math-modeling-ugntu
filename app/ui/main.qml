import QtQuick 2.5
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import QtQuick.Window 2.12

import "."

Item {
    width   : 1200
    height  : 900

    Rectangle {
        id           : backgroundRect
        color        : AppTheme.mainWindowBGColor
        anchors.fill : parent
    }

    StackView {
        id           : mainStackView
        anchors.fill : parent
        initialItem  : Qt.resolvedUrl("HomePage.qml")
    }

    Connections {
        target: Init
        onRequestPushPage: {
            mainStackView.push(Qt.resolvedUrl(pageUrl));
        }
        onRequestPopPage: {
            mainStackView.pop();
        }
    }
}

