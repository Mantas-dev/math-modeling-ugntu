import QtQuick 2.0
import QtQml 2.12
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Item {
    id : container
    implicitHeight  : childrenRect.height
    implicitWidth   : childrenRect.width

    property string buttonText          : "Long long text"
    property color  buttonTextColor     : AppTheme.mainButtonTextColor
    property color  buttonColor         : AppTheme.mainButtonColor
    property color  buttonClickColor    : AppTheme.mainButtonClickColor
    property color  buttonHoverColor    : AppTheme.mainButtonHoverColor
    property int    buttonHeight        : 0
    property int    buttonWidth         : 0
    property int    buttonTextPxSize    : 20

    QtObject {
        id: qtObj
        property color activeColor: buttonColor
    }

    signal clicked();

    Button {
        id : customButton
        style: ButtonStyle {
            background: Rectangle {
                color   : qtObj.activeColor
                radius  : 10
            }
            label: Text {
                color   : container.buttonTextColor
                text    : container.buttonText
                font {
                    family      : AppTheme.fontProximaNovaRegular.name
                    pixelSize   : container.buttonTextPxSize
                    bold        : true
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment : Text.AlignVCenter
            }
        }
    }

    MouseArea {
        id           : containerMA
        anchors.fill : parent
        hoverEnabled : true
        onClicked: {
            container.clicked()
        }
        onPressed: {
            qtObj.activeColor = container.buttonClickColor
        }
        onReleased: {
            if (containerMA.containsMouse)
                qtObj.activeColor = container.buttonHoverColor
            else
                qtObj.activeColor = container.buttonColor
        }
        onEntered: {
            if (!containerMA.pressed)
                qtObj.activeColor = container.buttonHoverColor
        }
        onExited: {
            if (!containerMA.pressed)
                qtObj.activeColor = container.buttonColor
        }
    }

    onButtonHeightChanged: {
        customButton.height = buttonHeight
    }

    onButtonWidthChanged: {
        customButton.width = buttonWidth
    }
}
