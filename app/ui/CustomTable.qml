import QtQuick 2.12
import QtQml 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3


Item {
    id      : container
    width   : columnsHeader.implicitWidth + rowsHeader.implicitWidth
    height  : columnsHeader.implicitHeight + rowsHeader.implicitHeight

    property var tableModel     : null
    property int borderWidth    : 1
    property int columnWidth    : 100
    property int rowHeight      : 40

    Rectangle {
        id : borderTableRect
        anchors.fill    : parent
        color           : "transparent"
        border.color    : "black"
        z               : 3
    }

    Rectangle {
        id : emptyRectangle
        anchors {
            top  : parent.top
            left : parent.left
        }

        color   : "#f0f0f0"
        width   : rowsHeader.width - container.borderWidth
        height  : columnsHeader.implicitHeight - container.borderWidth
    }


    Column {
        id: rowsHeader
        anchors {
            top     : emptyRectangle.bottom
            left    : parent.left
            right   : tableView.left
            bottom  : parent.bottom
        }

        Repeater {
            model : tableView.rows > 0 ? tableView.rows : 1
            Label {
                text                : tableView.model == null ? "Column" : tableView.model.headerData(modelData, Qt.Horizontal)
                width               : tableView.model == null ? 80 :(tableView.model.rowsHeadersMaxLength() * 9) + (padding * 2)
                height              : tableView.rowHeightProvider(modelData)
                color               : "black"
                font {
                    family      : AppTheme.fontProximaNovaRegular.name
                    pixelSize   : 15
                }
                padding             : 10
                verticalAlignment   : Text.AlignVCenter

                background: Rectangle {
                    color: "#f0f0f0"

                    Rectangle {
                        id : rowsRightBorder
                        anchors {
                            top     : parent.top
                            bottom  : parent.bottom
                            right   : parent.right
                        }
                        width: container.borderWidth
                        color: "black"
                    }

                    Rectangle {
                        id : rowsTopBorder
                        anchors {
                            top     : parent.top
                            left    : parent.left
                            right   : parent.right
                        }
                        height: container.borderWidth
                        color: "black"
                    }
                }
            }
        }
    }


    Row {
        id: columnsHeader
        anchors {
            top     : parent.top
            left    : emptyRectangle.right
            right   : parent.right
            bottom  : tableView.top
        }

        Repeater {
            model: tableView.columns > 0 ? tableView.columns : 1
            Label {
                width               : tableView.columnWidthProvider(modelData)
                height              : 35
                text                : tableView.model == null ? 80 : tableView.model.headerData(modelData, Qt.Vertical)
                color               : "black"
                font {
                    family      : AppTheme.fontProximaNovaRegular.name
                    pixelSize   : 15
                }
                padding             : 10
                verticalAlignment   : Text.AlignVCenter
                horizontalAlignment : Text.AlignHCenter

                background: Rectangle {
                    color: "#f0f0f0"

                    Rectangle {
                        id : colsLeftBorder
                        anchors {
                            top     : parent.top
                            bottom  : parent.bottom
                            left   : parent.left
                        }
                        width: container.borderWidth
                        color: "black"
                    }

                    Rectangle {
                        id : colsBottomBorder
                        anchors {
                            left    : parent.left
                            right   : parent.right
                            bottom  : parent.bottom
                        }
                        height: container.borderWidth
                        color: "black"
                    }
                }
            }
        }
    }

    TableView {
        id                  : tableView
        columnWidthProvider : function (column) { return container.columnWidth; }
        rowHeightProvider   : function (column) { return container.rowHeight; }
        anchors {
            fill        : parent
            leftMargin  : rowsHeader.implicitWidth
            topMargin   : columnsHeader.implicitHeight
        }
        boundsBehavior  : Flickable.StopAtBounds
        model           : container.tableModel
        delegate: Rectangle {
            color: 'white'
            TextField {
                anchors.fill: parent
                text        : display
                color       : 'black'
                font {
                    family      : AppTheme.fontProximaNovaRegular.name
                    pixelSize   : 15
                }
                onTextEdited        : model.display = this.text
                verticalAlignment   : Text.AlignVCenter
                horizontalAlignment : Text.AlignHCenter
            }

            Rectangle {
                id : cellsRightBorder
                anchors {
                    top     : parent.top
                    bottom  : parent.bottom
                    right   : parent.right
                }
                width: container.borderWidth
                color: "black"
            }

            Rectangle {
                id : cellsBottomBorder
                anchors {
                    bottom  : parent.bottom
                    left    : parent.left
                    right   : parent.right
                }
                height: container.borderWidth
                color: "black"
            }
        }
    }
}
