import QtQuick 2.5
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import QtQuick.Window 2.12

import "."

Item {
    width   : 1200
    height  : 900

    Column {
        id                  : buttonsGroup
        anchors.centerIn    : parent
        spacing             : 30

        CustomMainButton {
            buttonText      : "Симплексный метод"
            buttonHeight    : 50
            buttonWidth     : 250
            onClicked: {
                Init.requestPushPage("SimplexSolutionPage.qml")
            }
        }

        CustomMainButton {
            buttonText      : "Транспортная задача"
            buttonHeight    : 50
            buttonWidth     : 250
            onClicked: {
                Init.requestPushPage("TransportTaskSolutionPage.qml")
            }
        }
    }
}

