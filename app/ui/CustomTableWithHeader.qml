import QtQuick 2.12
import QtQml 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3


Item {
    id      : container
    width   : childrenRect.width
    height  : childrenRect.height

    property string headerText     : "Test header"
    property int    headerSize     : 18
    property var    tableModel     : null
    property int    borderWidth    : 1
    property int    columnWidth    : 100
    property int    rowHeight      : 40

    Text {
        id   : header
        text : headerText
        font {
            family      : AppTheme.fontProximaNovaRegular.name
            pixelSize   : container.headerSize
        }
    }

    CustomTable {
        id          : tableComponent
        tableModel  : container.tableModel
        borderWidth : container.borderWidth
        columnWidth : container.columnWidth
        rowHeight   : container.rowHeight
        anchors {
            top         : header.bottom
            topMargin   : 15
        }
    }
}
