pragma Singleton
import QtQuick 2.5
import QtQml 2.12


QtObject {
    property FontLoader fontProximaNovaRegular: FontLoader {
        source: "../../fonts/ProximaNova-Regular.ttf"
    }

    property FontLoader fontProximaNovaBold: FontLoader {
        source: "../../fonts/ProximaNova-Bold.ttf"
    }
    property color mainWindowBGColor     : "#E0E0E0"
    property color mainButtonColor       : "#336699"
    property color mainButtonClickColor  : "#26527F"
    property color mainButtonHoverColor  : "#3875B3"
    property color mainButtonTextColor   : "#FFFFFF"
    property color backButtonColor       : "#999999"
    property color backButtonClickColor  : "#7c7c7c"
    property color backButtonHoverColor  : "#aaaaaa"
    property color backButtonTextColor   : "#FFFFFF"
}
