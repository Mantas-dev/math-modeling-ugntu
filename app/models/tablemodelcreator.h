#ifndef TABLEMODELCREATOR_H
#define TABLEMODELCREATOR_H

#include <QObject>
#include <QVector>

#include "customtablemodel.h"

class TableModelCreator : public QObject
{
    Q_OBJECT
public:
    explicit TableModelCreator(QObject *parent = nullptr);
    ~TableModelCreator();

    CustomTableModel* createTableModel(int rows, int columns);
    CustomTableModel* createTableModel(QVector<QVector<double>> vectorData);
    CustomTableModel* createTableModel(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders);

    void deleteTables();
signals:

private:
    QVector <CustomTableModel *> tablesStorage;
};

#endif // TABLEMODELCREATOR_H
