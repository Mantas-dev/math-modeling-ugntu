#ifndef INITIALTABLEMODEL_H
#define INITIALTABLEMODEL_H

#include <QVector>
#include <QStringList>
#include <QAbstractTableModel>

class CustomTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit CustomTableModel();
    explicit CustomTableModel(int rows, int columns);
    explicit CustomTableModel(QVector<QVector<double>> vectorData);
    explicit CustomTableModel(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders);

    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    QVector<QVector<double>> getData();

    Q_INVOKABLE int rowsHeadersMaxLength();

private:
    void setTableData(QVector<QVector<double>> vectorData);
    void setTableData(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders);
    void setDefaultHeadersValue(int rowsSize, int columnsSize);
    QVector<QVector<double>> m_data;
    QStringList m_rowsHeaders;
    QStringList m_columnsHeaders;
    int m_rowsHeadersMaxLengthCache;
};

#endif // INITIALTABLEMODEL_H
