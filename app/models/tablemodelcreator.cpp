#include "tablemodelcreator.h"

TableModelCreator::TableModelCreator(QObject *parent) : QObject(parent)
{

}

TableModelCreator::~TableModelCreator()
{
    deleteTables();
}

CustomTableModel *TableModelCreator::createTableModel(int rows, int columns)
{
    CustomTableModel *tableModel = new CustomTableModel(rows, columns);
    tablesStorage.push_back(tableModel);
    return tableModel;
}

CustomTableModel *TableModelCreator::createTableModel(QVector<QVector<double>> vectorData)
{
    CustomTableModel *tableModel = new CustomTableModel(vectorData);
    tablesStorage.push_back(tableModel);
    return tableModel;
}

CustomTableModel *TableModelCreator::createTableModel(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders)
{
    CustomTableModel *tableModel = new CustomTableModel(vectorData, rowsHeaders, columnHeaders);
    tablesStorage.push_back(tableModel);
    return tableModel;
}

void TableModelCreator::deleteTables()
{
    qDeleteAll(tablesStorage.begin(), tablesStorage.end());
    tablesStorage.clear();
}
