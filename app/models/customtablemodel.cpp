#include "customtablemodel.h"

CustomTableModel::CustomTableModel() : QAbstractTableModel()
{

}

CustomTableModel::CustomTableModel(int rows, int columns)
{
    m_data.resize(rows);

    for (auto &rowVector : m_data){
        rowVector.resize(columns);
    }

    setDefaultHeadersValue(rows, columns);
    m_rowsHeadersMaxLengthCache = 0;
}

CustomTableModel::CustomTableModel(QVector<QVector<double>> vectorData)
{
    setTableData(vectorData);
}

CustomTableModel::CustomTableModel(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders)
{
    setTableData(vectorData, rowsHeaders,columnHeaders);
}

int CustomTableModel::rowCount(const QModelIndex &parent) const
{
    return m_data.size();
}

int CustomTableModel::columnCount(const QModelIndex &parent) const
{
    if (m_data.size() == 0)
        return 0;

    return m_data.at(0).size();
}

QVariant CustomTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_data.size())
        return QVariant();

    if (index.column() >= m_data.at(index.row()).size())
        return QVariant("");


    if (role != Qt::DisplayRole)
        return QVariant();


    double roundedValue = round(m_data.at(index.row()).at(index.column()) * 100.0) / 100.0;

    return roundedValue;

}

QVariant CustomTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
        return m_rowsHeaders.at(section);
    else
        return m_columnsHeaders.at(section);
}

bool CustomTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role);

    if (index.row() >= m_data.size())
        return false;

    if (index.column() >= m_data.at(index.row()).size())
        return false;

    m_data[index.row()][index.column()] = value.toInt();

    return true;
}

QVector<QVector<double> > CustomTableModel::getData()
{
    return m_data;
}

int CustomTableModel::rowsHeadersMaxLength()
{
    if (m_rowsHeadersMaxLengthCache != 0)
        return m_rowsHeadersMaxLengthCache;

    for (QString rowHeader : m_rowsHeaders){
        m_rowsHeadersMaxLengthCache = qMax(m_rowsHeadersMaxLengthCache, rowHeader.length());
    }

    return m_rowsHeadersMaxLengthCache;
}

void CustomTableModel::setTableData(QVector<QVector<double>> vectorData)
{
    m_data = vectorData;
    if (vectorData.size() != 0)
        setDefaultHeadersValue(vectorData.size(), vectorData.at(0).size());
}

void CustomTableModel::setTableData(QVector<QVector<double>> vectorData, QStringList rowsHeaders, QStringList columnHeaders)
{
    m_data = vectorData;
    m_rowsHeaders = rowsHeaders;
    m_columnsHeaders = columnHeaders;
    m_rowsHeadersMaxLengthCache = 0;
}

void CustomTableModel::setDefaultHeadersValue(int rowsSize, int columnsSize)
{
    for (int i = 0; i < rowsSize; i++)
        m_rowsHeaders.push_back(QString::number(i + 1));

    for (int j = 0; j < columnsSize; j++)
        m_columnsHeaders.push_back(QString::number(j + 1));
}
