#include "simplex.h"

Simplex::Simplex()
{

}

void Simplex::calculate(QVector<QVector<double>> initialData)
{
    prepareAllData(initialData);

    m_simplexData = createFirstTable(initialData);

    int createdTablesCount = 1;
    int maxNegativeCoeffIndex = findMaxNegativeCoeff(m_simplexData);

    //Пока имеются отрицательные элементы коэффициентов целевой функции
    while (maxNegativeCoeffIndex != -1) {
        //Определить разрешающий элемент, его положение и выполнить обмен свободной и базисной переменных
        createdTablesCount++;
        int rowOfElemForSwap = rowIndexOfMinElem(m_simplexData, maxNegativeCoeffIndex);
        double permittingElement = m_simplexData.at(rowOfElemForSwap).at(maxNegativeCoeffIndex);
        doSwap(m_simplexData, permittingElement, rowOfElemForSwap, maxNegativeCoeffIndex);
        emit tableCreated(QString("Симплекс-таблица №%1").arg(createdTablesCount), m_simplexData, m_rowsHeaders, m_colsHeaders);
        maxNegativeCoeffIndex = findMaxNegativeCoeff(m_simplexData);
    }

    sendText(QString("Текущий опорный план %1 является оптимальным.").arg(basePlanToString()));
    sendText(QString("<b>Значение целевой функции F(X) =</b> %1").arg(m_simplexData.last().last()));
}

void Simplex::clear()
{
    m_basePlan.clear();
    m_limits.clear();
    m_objectiveFuncCoeffs.clear();
    m_rowsHeaders.clear();
    m_colsHeaders.clear();
}

/**
 * @brief Simplex::findMaxNegativeCoeff
 * Находит максимальный отрицательный коэффициент целевой функции в последней строке симплекс-таблицы
 * @param simplexData - симплекс-таблица
 * @return Индекс столбца максимального отрицательного элемента. Возвращает -1, если нет ни одного отрицательного элемента
 */

int Simplex::findMaxNegativeCoeff(QVector<QVector<double>> &simplexData)
{
    double maxNegative = 0;

    for (double value : simplexData.last()){
        if (value < 0) {
            maxNegative = fmax(maxNegative, qAbs(value));
        }
    }

    if (maxNegative == 0)
        return -1;

    return simplexData.last().indexOf(maxNegative * -1);
}

/**
 * @brief Simplex::rowIndexOfMinElem
 * Находит строку с минимальным элементом в выбранном столбце
 * @param simplexData - симплекс-таблица
 * @param columnIndex - столбец для поиска
 * @return индекс найденной строки
 */

int Simplex::rowIndexOfMinElem(QVector<QVector<double> > &simplexData, int columnIndex)
{
    double minValue = DBL_MAX, index = 0;

    for (int i = 0; i < simplexData.size() - 1; i++) {
        if (simplexData.at(i).at(columnIndex) > 0){
            double currentValue = simplexData.at(i).last() / simplexData.at(i).at(columnIndex);

            if (currentValue < minValue) {
                minValue = currentValue;
                index = i;
            }
        }
    }

    return index;
}

/**
 * @brief Simplex::doSwap
 * Меняет местами свободную переменную и переменную базиса
 * @param simplexData - симплекс-таблица
 * @param permittingElement - разрешающий элемент
 * @param row - индекс строки с разрешающим элементом
 * @param column - индекс столбца с разрешающим элементом
 */

void Simplex::doSwap(QVector<QVector<double>> &simplexData, double permittingElement, int row, int column)
{
    //Сохранить текущую таблицу в локальный вектор
    QVector<QVector<double>> oldSimplexData = simplexData;

    //Поменять заголовки с переменными местами
    m_rowsHeaders[row] = m_colsHeaders.at(column);

    //Заполнить элементы разрешающего столбца нулями
    for (int i = 0; i < simplexData.size(); i++){
        simplexData[i][column] = 0;
    }

    //Заполнить элементы разрешающей строки выражением <текущее_значение>/<разрешающий_элемент>
    for (int j = 0; j < simplexData.at(row).size(); j++){
        if (j != column)
            simplexData[row][j] = simplexData.at(row).at(j) / permittingElement;
    }

    //В ячейку с разрешающим элементом записать 1
    simplexData[row][column] = 1;

    //Все остальные элементы в таблице найти по правилу прямоугольника
    for (int i = 0; i < simplexData.size(); i++){
        for (int j = 0; j < simplexData.at(i).size(); j++){
            if (i != row && j != column){
                simplexData[i][j] = ((oldSimplexData.at(row).at(column) * oldSimplexData.at(i).at(j)) -
                                     (oldSimplexData.at(row).at(j) * oldSimplexData.at(i).at(column))) / permittingElement;
            }
        }
    }

    //Обновить значения для опорного плана. Для свободных переменных пишется 0, для переменных в базисе пишется соответствующее значение из столбца базиса
    for (QMap<QString, double>::const_iterator iter = m_basePlan.cbegin(); iter != m_basePlan.cend(); ++iter){
        int index = m_rowsHeaders.indexOf(iter.key());
        if (index != -1)
            m_basePlan[iter.key()] = m_simplexData.at(index).last();
        else
            m_basePlan[iter.key()] = 0;
    }
}

/**
 * @brief Simplex::prepareAllData
 * Парсит таблицу и заполняет и необходимые данные
 * @param initialData - исходная таблица
 */

void Simplex::prepareAllData(QVector<QVector<double>> &initialData)
{
    clear();

    m_objectiveFuncCoeffs.resize(initialData.size() - 1);
    m_objectiveFuncCoeffs.fill(0);

    //Заполнить ограничения, базовый план и названия строк симплекс-таблицы
    for (int i = 0; i < initialData.size() - 1; i++){
        const double limitValue = initialData.at(i).last();
        m_limits.push_back(limitValue);
        QString variableX = QString("x%1").arg((initialData.first().size() - 1) + i + 1);
        m_rowsHeaders << variableX;
        m_basePlan[variableX] = limitValue;
    }

    //Заполнить коэффициенты целевой функции
    for (int j = 0; j < initialData.last().size(); j++){
        m_objectiveFuncCoeffs[j] = initialData.last().at(j) * -1;
    }

    //Заполнить названия столбцов симплекс-таблицы
    for (int i = 0; i < m_objectiveFuncCoeffs.size(); i++) {
        QString variableX = QString("x%1").arg(i + 1);
        m_colsHeaders << variableX;
        m_basePlan[variableX] = 0;
    }

    m_colsHeaders << m_rowsHeaders;
    m_colsHeaders << "Bi";
    m_rowsHeaders << "F";
}

/**
 * @brief Simplex::createFirstTable
 * Создает первую симплекс таблицу из таблицы с исходными данными
 * @param initialData - Вектор с исходными данными
 * @return Вектор симплекс таблицы
 */

QVector<QVector<double>> Simplex::createFirstTable(QVector<QVector<double>> &initialData)
{
    QVector<QVector<double>> simplexTable;
    simplexTable.resize(initialData.size());

    //Заполнить вектор значениями исходной таблицы
    for (int i = 0; i < initialData.size() - 1; i++){
        //Заполнить значениями столбцов, не добавляя последний столбец
        for (int j = 0; j < initialData.at(i).size() - 1; j++){
            simplexTable[i].push_back(initialData.at(i).at(j));
        }

        //Добавить коэффициенты для базисных переменных
        for (int z = 0; z < initialData.size() - 1; z++){
            double basisCoeff = (z == i) ? 1 : 0;
            simplexTable[i].push_back(basisCoeff);
        }

        //Добавить в конец ограничение текущей строки
        simplexTable[i].push_back(m_limits.at(i));
    }

    //Изменить количество столбцов в последней строке и заполнить нулями
    simplexTable.last().resize(simplexTable.first().size());
    simplexTable.last().fill(0);

    //Заполнить последнюю строку коэффициентами целевой функции
    for (int i = 0; i < m_objectiveFuncCoeffs.size(); i++){
        simplexTable.last()[i] = m_objectiveFuncCoeffs.at(i);
    }

    emit tableCreated("Симплекс-таблица №1", simplexTable, m_rowsHeaders, m_colsHeaders);

    return simplexTable;
}

/**
 * @brief Simplex::basePlanToString
 * Преобразовать QMap с опорным планом к строке формата "X = {x1, x2, ..., xn}"
 * @return Строка с опорным планом
 */

QString Simplex::basePlanToString()
{
    QString basePlanStr = "X = {";

    for (QMap<QString, double>::iterator iter = m_basePlan.begin(); iter != m_basePlan.end(); ++iter) {
        basePlanStr += QString::number(iter.value());
        basePlanStr += ", ";
    }

    basePlanStr = basePlanStr.left(basePlanStr.length() - 2);
    basePlanStr += "}";

    return basePlanStr;
}
