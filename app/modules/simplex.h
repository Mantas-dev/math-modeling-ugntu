#ifndef SIMPLEX_H
#define SIMPLEX_H

#include <QObject>
#include <QMap>

class Simplex : public QObject
{
    Q_OBJECT
public:
    Simplex();

    void calculate(QVector<QVector<double>> initialData);

signals:
    void tableCreated(QString title, QVector<QVector<double>> &vectorData,
                      QStringList rowsHeaders, QStringList columnHeaders);
    void sendText(QString text);

private:
    void clear();
    int findMaxNegativeCoeff(QVector<QVector<double>> &simplexData);
    int rowIndexOfMinElem(QVector<QVector<double>> &simplexData, int columnIndex);
    void doSwap(QVector<QVector<double>> &simplexData,double permittingElement, int row, int column);
    void prepareAllData(QVector<QVector<double>> &initialData);
    QVector<QVector<double>> createFirstTable(QVector<QVector<double>> &initialData);
    QString basePlanToString();

    QMap<QString, double> m_basePlan; //Опорный план
    QVector<double> m_limits; //Ограничения
    QVector<double> m_objectiveFuncCoeffs; //Коэффициенты целевой функции

    QStringList  m_rowsHeaders; //Заголовки строк
    QStringList  m_colsHeaders; //Заголовки столбцов

    QVector<QVector<double>> m_simplexData;
};

#endif // SIMPLEX_H
