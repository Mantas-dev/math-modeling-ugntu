#ifndef MODELMANAGER_H
#define MODELMANAGER_H

#include <QObject>

#include "../models/customtablemodel.h"
#include "../models/tablemodelcreator.h"
#include "../modules/simplex.h"
#include "../modules/transporttask.h"

class ModelManager : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QObject* simplexTableModel READ getSimplexTableModel CONSTANT)
    Q_PROPERTY(QObject* transportTableModel READ getTransportTableModel CONSTANT)

    ModelManager();
    ~ModelManager();

    void createTable(const QString title, const int rows, const int columns);
    void createTable(const QString title, const QVector<QVector<double>> &vectorData);
    void createTable(const QString title, const QVector<QVector<double>> &vectorData,
                     QStringList rowsHeaders, QStringList columnHeaders);

    void deleteTables();

    void initSimplexTableModel();
    void initTransportTableModel();

    QObject* getSimplexTableModel();
    QObject* getTransportTableModel();

    TableModelCreator* getTableModelCreator();

    Q_INVOKABLE void resolveBySimplex();
    Q_INVOKABLE void resolveTransportTask();

signals:
    void tableModelCreated(QString title, QObject *model);
    void sendText(QString text);

private:
    TableModelCreator *m_pTableModelCreator;
    CustomTableModel  *m_pSimplexTableModel, *m_pTransportTableModel;
    Simplex           *m_pSimplexCalculateModule;
    TransportTask     *m_pTransportTaskModule;
};

#endif // MODELMANAGER_H
