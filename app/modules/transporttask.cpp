#include "transporttask.h"

TransportTask::TransportTask()
{

}

void TransportTask::calculate(QVector<QVector<double>> initialData)
{
    prepareAllData(initialData);
    createBasePlanNorthWest(initialData);
    emit tableCreated("Опорный план перевозок", initialData, m_rowsHeaders, m_colsHeaders);
    solveByMethodOfPotentials(initialData);
    emit tableCreated("Оптимальный план перевозок", initialData, m_rowsHeaders, m_colsHeaders);
    sendText(QString("<b>Значение целевой функции F(X) =</b> %1").arg(calculateObjectiveFunc(initialData, m_tariffsMatrix)));
}

/**
 * @brief TransportTask::prepareAllData
 * Парсит таблицу и заполняет и необходимые данные
 * @param initialData - исходная таблица
 */

void TransportTask::prepareAllData(QVector<QVector<double>> &initialData)
{
    if (initialData.last().size() == initialData.first().size()) {
        initialData.last().pop_back();
    }

    initialData.last().push_back(0);

    //Определить сумму потребностей и запасов
    double stocksSum = getRowsSum(initialData, initialData.first().size() - 1);
    double needsSum = getColsSum(initialData, initialData.size() - 1);

    //Если сумма запасов больше - добавить фиктивный столбец
    //Если сумма потребностей больше - добавить фиктивную строку
    if (stocksSum > needsSum) {
        for (int i = 0; i < initialData.size(); i++) {
            initialData[i].insert(initialData.at(i).size() - 1, 0);
        }

        initialData[initialData.size() - 1][initialData.first().size() - 2] = stocksSum - needsSum;

    } else if (needsSum > stocksSum) {
        QVector<double> fakeStocks(initialData.first().size() - 1);
        fakeStocks.fill(0);
        fakeStocks[fakeStocks.size() - 1] = needsSum - stocksSum;

        initialData.insert(initialData.size() - 1, fakeStocks);
    }

    //Заполнить заголовки строк и столбцов таблицы
    for (int i = 0; i < initialData.size() - 1; i++) {
        m_rowsHeaders << QString("A%1").arg(i + 1);
    }

    m_rowsHeaders << "Потребности";

    for (int j = 0; j < initialData.first().size() - 1; j++) {
        m_colsHeaders << QString("B%1").arg(j + 1);
    }

    m_colsHeaders << "Запасы";

    //Заполнить матрицу тарифов
    m_tariffsMatrix = initialData;
    m_tariffsMatrix.removeLast();

    for (int i = 0; i < m_tariffsMatrix.size(); i++){
        m_tariffsMatrix[i].removeLast();
    }
}

/**
 * @brief TransportTask::createBasePlanNorthWest
 * Создает опорный план по методу северо-западного угла
 * @param transportData - таблица с тарифами и ресурсами
 */

void TransportTask::createBasePlanNorthWest(QVector<QVector<double> > &transportData)
{
    int currentRow = 0, currentCol = 0;
    QVector<double> stocksData;
    QVector<double> needsData = transportData.last(); //Сохраняет строку с потребностями

    //Сохраняет столбец с запасами
    for (int i = 0; i < transportData.size(); i++){
        stocksData.push_back(transportData.at(i).last());
    }

    //Двигать в северо-западном направлении, пока не будет достигнут конц таблицы
    while (currentRow != transportData.size() - 1 && currentCol != transportData.last().size() - 1) {
        //Запомнить координаты текущих потребностей и запасов
        QVector<int> currStocksCoords = {currentRow, transportData.at(currentRow).size() - 1};
        QVector<int> currNeedsCoords = {transportData.size() - 1, currentCol};

        //Получить значения этих потребностей и запасов
        double currStocksValue = transportData.at(currStocksCoords.at(0)).at(currStocksCoords.at(1));
        double currNeedsValue = transportData.at(currNeedsCoords.at(0)).at(currNeedsCoords.at(1));

        //Если запасов меньше чем потребностей
        if (currStocksValue < currNeedsValue){
            transportData[currentRow][currentCol] = currStocksValue; //Записать в текущую ячейку значение запасов
            //Заполнить все оставшиеся ячейки в строке нулями
            for (int j = currentCol + 1; j < transportData.at(currentRow).size() - 1; j++)
                transportData[currentRow][j] = 0;

            //Вычесть из текущих запасов и потребностей значение запасов
            transportData[currStocksCoords.at(0)][currStocksCoords.at(1)] -= currStocksValue;
            transportData[currNeedsCoords.at(0)][currNeedsCoords.at(1)] -= currStocksValue;

            currentRow++;
        } else { //Если потребностей меньше
            transportData[currentRow][currentCol] = currNeedsValue;
            for (int i = currentRow + 1; i < transportData.size() - 1; i++)
                transportData[i][currentCol] = 0;

            //Вычесть значение потребностей
            transportData[currNeedsCoords.at(0)][currNeedsCoords.at(1)] -= currNeedsValue;
            transportData[currStocksCoords.at(0)][currStocksCoords.at(1)] -= currNeedsValue;

            currentCol++;
        }
    }

    //Повторно записать в таблицу запасы и потребности
    transportData[transportData.size() - 1] = needsData;

    for (int i = 0; i < transportData.size(); i++){
        transportData[i][transportData.at(i).size() - 1] = stocksData[i];
    }
}

/**
 * @brief TransportTask::solveByMethodOfPotentials
 * Выполняет поиск оптимального решения методом потенциалов
 * @param transportData - опорный план транспортной задачи
 */

void TransportTask::solveByMethodOfPotentials(QVector<QVector<double>> &transportData)
{
    int createdTablesCount = 0;
    fillPotentials(transportData); //Вычислить потенциалы
    QPair<int, int> negativeGradeCoords = findNegativeGrades(transportData); //Найти координаты с максимальной отрицательной оценкой

    //Пока есть отрицательные оценки
    while (negativeGradeCoords != QPair<int, int>(-1, -1)) {
        createdTablesCount++;
        //Найти цикл пересчета
        auto findedCycle = findCycle(transportData, negativeGradeCoords.first, negativeGradeCoords.second);
        recalculateTransportData(transportData, findedCycle); //Пересчитать таблицу по циклу
        fillPotentials(transportData);
        negativeGradeCoords = findNegativeGrades(transportData);
        emit tableCreated(QString("План перевозок №%1, полученный методом потенциалов").arg(createdTablesCount),
                          transportData, m_rowsHeaders, m_colsHeaders);
    }
}

/**
 * @brief TransportTask::fillPotentials
 * Определяет потенциалы для опорного плана
 * @param transportData - опорный план транспортной задачи
 */

void TransportTask::fillPotentials(const QVector<QVector<double>> &transportData)
{
    QStringList rowPotentialsList, colPotentialsList;

    m_rowPotentials.clear();
    m_colPotentials.clear();

    for (int i = 0; i < transportData.size() - 1; i++){
        rowPotentialsList << "";
    }

    //Для первой строки определяем нулевой потенциал
    rowPotentialsList[0] = "0";

    //Находим потенциалы для столбцов на первой строке
    for (int j = 0; j < transportData.first().size() - 1; j++){
        if (transportData.first().at(j) != 0)
            colPotentialsList << QString::number(m_tariffsMatrix.first().at(j) - 0);
        else
            colPotentialsList << "";
    }

    int rowIndex = 1;

    //Пока существуют ненайденные потенциалы для строк и столбцов
    while (rowPotentialsList.contains("") || colPotentialsList.contains("")) {
        //Пройтись по элементам строки
        for (int j = 0; j < transportData.at(rowIndex).size() - 1; j++){
            if (transportData.at(rowIndex).at(j) != 0){
                //Пропустить, если еще не найдены потенциалы для текущей строки и столбца
                if (rowPotentialsList.at(rowIndex).isEmpty() && colPotentialsList.at(j).isEmpty())
                    continue;

                //Если есть потенциал для строки, то найти потенциал для столбца (из тарифа вычесть потенциал строки),
                //иначе найти потенциал для строки
                if (!rowPotentialsList.at(rowIndex).isEmpty())
                    colPotentialsList[j] = QString::number(m_tariffsMatrix.at(rowIndex).at(j) - rowPotentialsList.at(rowIndex).toInt());
                else
                    rowPotentialsList[rowIndex] = QString::number(m_tariffsMatrix.at(rowIndex).at(j) - colPotentialsList.at(j).toInt());
            }
        }

        rowIndex++;

        if (rowIndex > transportData.size() - 2)
            rowIndex = 1;
    }

    //Сохранить числовые значения потенциалов
    for (QString rowPotential : rowPotentialsList) {
        m_rowPotentials.push_back(rowPotential.toDouble());
    }

    for (QString colPotential : colPotentialsList) {
        m_colPotentials.push_back(colPotential.toDouble());
    }
}

/**
 * @brief TransportTask::recalculateTransportData
 * Пересчитывает опорный план по заданному циклу
 * @param transportData - опорный план транспортной задачи
 * @param cycle - цикл пересчета
 */

void TransportTask::recalculateTransportData(QVector<QVector<double> > &transportData, const QList<QPair<QVector<int>, QString> > cycle)
{
    double minValue = getMinValueFromCycle(transportData, cycle);

    for (auto item : cycle){
        if (item.second == "+") {
            transportData[item.first.at(0)][item.first.at(1)] += minValue;
        } else {
            transportData[item.first.at(0)][item.first.at(1)] -= minValue;
        }
    }
}

/**
 * @brief TransportTask::findNegativeGrades
 * Находит координаты наибольшей отрицательной оценки свободных клеток
 * @param transportData - опорный план транспортной задачи
 * @return координаты оценки. Если оценка не найдена, возвращает (-1;-1)
 */

QPair<int, int> TransportTask::findNegativeGrades(const QVector<QVector<double> > &transportData)
{
    double maxElem = 0;
    QPair<int, int> coordinates(-1, -1);

    for (int i = 0; i < transportData.size() - 1; i++) {
        for (int j = 0; j < transportData.at(i).size() - 1; j++) {
            if (transportData.at(i).at(j) == 0) {
                double grade = (m_rowPotentials.at(i) + m_colPotentials.at(j)) - m_tariffsMatrix.at(i).at(j);
                if (grade > 0 && grade > maxElem) {
                    maxElem = grade;
                    coordinates.first = i;
                    coordinates.second = j;
                }
            }
        }
    }

    return coordinates;
}

/**
 * @brief TransportTask::findCycle
 * Составляет цикл в опорном плане
 * @param transportData - опорный план транспортной задачи
 * @param row - исходная строка
 * @param column - исходный столбец
 * @return
 */

QList<QPair<QVector<int>, QString>> TransportTask::findCycle(const QVector<QVector<double> > &transportData, int row, int column)
{
    int currentSearchingPlace = SearchingPlace::ROW; //Направление поиска (по столбцу/по строке)
    QList<QPair<QVector<int>, QString>> cyclePath;
    QList<QPair<QVector<int>, QVector<int>>> coordsAndIndexes; //Список пар с координатами и индексами ненулевых элементов
    QVector<int> currentCoords = {row, column};
    QString currentSign = "+";

    while (true) {
        //Если найденный элемент завершает построение цикла
        if (coordsAndIndexes.size() > 0 && isCycleCompleted(coordsAndIndexes, currentCoords)) {
            coordsAndIndexes.append(QPair<QVector<int>, QVector<int>>(currentCoords, QVector<int>()));
            break;
        }

        //Если поиск осуществляется по строке
        if (currentSearchingPlace == SearchingPlace::ROW) {
            //Найти индексы всех других ненулевы элементов в строке
            QVector<int> findedColsIndexes = getIndexesOfNonZeroValuesInRow(transportData, currentCoords.at(0), currentCoords.at(1));

            //Если индексы не найдены - удалить все координаты с пустым вектором индексов
            if (findedColsIndexes.isEmpty() && coordsAndIndexes.size() > 0) {
                doRemove(coordsAndIndexes, currentCoords, currentSearchingPlace);
            } else {
                //Забрать первый индекс из вектора. Записать пару <текущие координаты>:<найденные индексы>.
                //Перейти на координаты с новым индексом. Поменять направление поиска
                int colIndex = findedColsIndexes.takeFirst();
                coordsAndIndexes.append(QPair<QVector<int>, QVector<int>>(currentCoords, findedColsIndexes));
                currentCoords[1] = colIndex;
                currentSearchingPlace = SearchingPlace::COLUMN;
            }
        } else { //Если поиск осуществляется по столбцу
            QVector<int> findedRowsIndexes = getIndexesOfNonZeroValuesInCol(transportData, currentCoords.at(0), currentCoords.at(1));

            if (findedRowsIndexes.isEmpty() && coordsAndIndexes.size() > 0) {
                doRemove(coordsAndIndexes, currentCoords, currentSearchingPlace);
            } else {
                int rowIndex = findedRowsIndexes.takeFirst();
                coordsAndIndexes.append(QPair<QVector<int>, QVector<int>>(currentCoords, findedRowsIndexes));
                currentCoords[0] = rowIndex;
                currentSearchingPlace = SearchingPlace::ROW;
            }
        }
    }

    //Пройтись по всему списку пар и записать все координаты в список цикла
    for (auto item : coordsAndIndexes){
        QVector<int> coords = {item.first.at(0), item.first.at(1)};
        cyclePath.append(QPair<QVector<int>, QString>(coords, currentSign));
        switchSign(currentSign);

    }

    return cyclePath;
}

/**
 * @brief TransportTask::getIndexesOfNonZeroValuesInRow
 * Возвращает индексы столбцов ненулевых элементов в строке, не считая указанного столбца
 * @param transportData - опорный план транспортной задачи
 * @param row - строка для поиска
 * @param column - столбец, который необходимо пропускать
 * @return Вектор с индексами столбцов
 */

QVector<int> TransportTask::getIndexesOfNonZeroValuesInRow(const QVector<QVector<double> > &transportData, int row, int column)
{
    QVector<int> indexes;

    for (int j = 0; j < transportData.at(row).size() - 1; j++){
        if (j != column && transportData.at(row).at(j) != 0) {
            indexes.push_back(j);
        }
    }

    return indexes;
}

/**
 * @brief TransportTask::getIndexesOfNonZeroValuesInCol
 * Возвращает индексы строк ненулевых элементов в столбце, не считая указанной строки
 * @param transportData - опорный план транспортной задачи
 * @param row - строка, которую необходимо пропускать
 * @param column - столбец для поиска
 * @return Вектор с индексами строк
 */

QVector<int> TransportTask::getIndexesOfNonZeroValuesInCol(const QVector<QVector<double> > &transportData, int row, int column)
{
    QVector<int> indexes;

    for (int i = 0; i < transportData.size() - 1; i++){
        if (i != row && transportData.at(i).at(column) != 0) {
            indexes.push_back(i);
        }
    }

    return indexes;

}

void TransportTask::switchSign(QString &sign)
{
    if (sign == "+") {
        sign = "-";
    } else {
        sign = "+";
    }
}

/**
 * @brief TransportTask::isCycleCompleted
 * Проверяет, приводят ли текущие координаты к завершению цикла
 * @param coordsAndIndexes - список координат цикла и найденных индексов строк/столбцов
 * @param currentCoords - текущие координаты
 * @return Результат проверки на то, является ли цикл полным
 */

bool TransportTask::isCycleCompleted(const QList<QPair<QVector<int>, QVector<int>>> coordsAndIndexes, const QVector<int> &currentCoords)
{
   auto beginCoords = coordsAndIndexes.first().first;

   if ((coordsAndIndexes.size() >= 3 && coordsAndIndexes.size() % 2 != 0) &&
           (beginCoords.at(0) == currentCoords.at(0) || beginCoords.at(1) == currentCoords.at(1))) {
       return true;
   }

   return false;
}

/**
 * @brief TransportTask::doRemove
 * Выполняет удаление всех координат, у которых вектор с индексами строк/столбцов является пустым
 * @param coordsAndIndexes - список координат цикла с индексами строк/столбцов
 * @param currentCoords - текущие координаты
 * @param currentSearchingPlace - текщее направление для поиска
 */

void TransportTask::doRemove(QList<QPair<QVector<int>, QVector<int>>> &coordsAndIndexes, QVector<int> &currentCoords, int &currentSearchingPlace)
{
    while (coordsAndIndexes.begin() != coordsAndIndexes.end() && coordsAndIndexes.last().second.isEmpty()){
        coordsAndIndexes.removeLast();
    }

    //Установить новое направление поиска
    if (coordsAndIndexes.size() % 2 == 0) {
        currentSearchingPlace = SearchingPlace::COLUMN;
    } else {
        currentSearchingPlace = SearchingPlace::ROW;
    }

    auto &lastElem = coordsAndIndexes.last();

    //Если поиск осуществлялся по строке, то после удаления следующий поиск должен идти по столбцу
    if (currentSearchingPlace == SearchingPlace::ROW) {
        int colIndex = lastElem.second.takeFirst();
        currentCoords[0] = lastElem.first.at(0);
        currentCoords[1] = colIndex;
        currentSearchingPlace = SearchingPlace::COLUMN;
    } else {
        int rowIndex = lastElem.second.takeFirst();
        currentCoords[0] = rowIndex;
        currentCoords[1] = lastElem.first.at(1);
        currentSearchingPlace = SearchingPlace::ROW;
    }
}

/**
 * @brief TransportTask::getMinValueFromCycle
 * Вовзращает минимальное значение элементов со знаком "-" в цикле
 * @param transportData - опорный план транспортной задачи
 * @param cycle - цикл пересчета
 * @return минимальное значение
 */

double TransportTask::getMinValueFromCycle(const QVector<QVector<double> > &transportData, const QList<QPair<QVector<int>, QString> > cycle)
{
    double minValue = DBL_MAX;

    for (auto item : cycle){
        if (item.second == "-") {
            auto coords = item.first;
            minValue = fmin(minValue, transportData.at(coords.at(0)).at(coords.at(1)));
        }
    }

    return minValue;
}

double TransportTask::getColsSum(const QVector<QVector<double>> &transportData, int row)
{
    double sum = 0;

    for (int j = 0; j < transportData.at(row).size(); j++)
        sum += transportData.at(row).at(j);

    return sum;
}

double TransportTask::getRowsSum(const QVector<QVector<double>> &transportData, int column)
{
    double sum = 0;

    for (int i = 0; i < transportData.size(); i++)
        sum += transportData.at(i).at(column);

    return sum;
}

/**
 * @brief TransportTask::calculateObjectiveFunc
 * Выполняет расчет целевой функции. Каждый элемент опорного плана умножается на соответствующий элемент
 * матрицы тарифов и суммируется
 * @param transportData - опорный план транспортной задачи
 * @param tariffsMatrix - матрица тарифов
 * @return значение целевой функции
 */

double TransportTask::calculateObjectiveFunc(const QVector<QVector<double>> &transportData, const QVector<QVector<double>> &tariffsMatrix)
{
    double totalSum = 0, colsSum = 0;
    for (int i = 0; i < transportData.size() - 1; i++){
        colsSum = 0;
        for (int j = 0; j < transportData.at(i).size() - 1; j++) {
            colsSum += transportData.at(i).at(j) * tariffsMatrix.at(i).at(j);
        }
        totalSum += colsSum;
    }

    return totalSum;
}
