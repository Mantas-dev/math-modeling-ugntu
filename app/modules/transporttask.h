#ifndef TRANSPORTTASK_H
#define TRANSPORTTASK_H

#include <QObject>
#include <QHash>
#include <QMap>
#include <QPair>
#include <QScopedPointer>

enum SearchingPlace {
    ROW,
    COLUMN
};

class TransportTask : public QObject
{
    Q_OBJECT
public:
    explicit TransportTask();

    void calculate(QVector<QVector<double>> initialData);

signals:
    void tableCreated(QString title, QVector<QVector<double>> &vectorData,
                      QStringList rowsHeaders, QStringList columnHeaders);
    void sendText(QString text);


private:
    void prepareAllData(QVector<QVector<double>> &initialData);
    void createBasePlanNorthWest(QVector<QVector<double>> &transportData);
    void solveByMethodOfPotentials(QVector<QVector<double>> &transportData);
    void fillPotentials(const QVector<QVector<double>> &transportData);
    void recalculateTransportData(QVector<QVector<double>> &transportData, const QList<QPair<QVector<int>, QString>> cycle);

    QPair<int, int> findNegativeGrades(const QVector<QVector<double>> &transportData);

    QList<QPair<QVector<int>, QString>> findCycle(const QVector<QVector<double>> &transportData, int row, int column);
    QVector<int> getIndexesOfNonZeroValuesInRow(const QVector<QVector<double>> &transportData, int row, int column);
    QVector<int> getIndexesOfNonZeroValuesInCol(const QVector<QVector<double>> &transportData, int row, int column);
    void switchSign(QString &sign);
    bool isCycleCompleted(const QList<QPair<QVector<int>, QVector<int>>> coordsAndIndexes, const QVector<int> &currentCoords);
    void doRemove(QList<QPair<QVector<int>, QVector<int>>> &coordsAndIndexes, QVector<int> &currentCoords, int &currentSearchingPlace);
    double getMinValueFromCycle(const QVector<QVector<double>> &transportData, const QList<QPair<QVector<int>, QString>> cycle);

    double getColsSum(const QVector<QVector<double>> &transportData, int row);
    double getRowsSum(const QVector<QVector<double>> &transportData, int column);
    double calculateObjectiveFunc(const QVector<QVector<double>> &transportData, const QVector<QVector<double>> &tariffsMatrix);

    QVector<QVector<double>> m_transportData, m_tariffsMatrix;
    QVector<double> m_rowPotentials, m_colPotentials;
    QStringList m_colsHeaders, m_rowsHeaders;
};

#endif // TRANSPORTTASK_H
