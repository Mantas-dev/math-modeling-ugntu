#include "modelmanager.h"

ModelManager::ModelManager()
{
    m_pSimplexTableModel = new CustomTableModel();
    m_pTableModelCreator = new TableModelCreator();
    m_pSimplexCalculateModule = new Simplex();
    m_pTransportTaskModule = new TransportTask();

    connect(m_pSimplexCalculateModule, &Simplex::sendText, this, &ModelManager::sendText);

    connect(m_pSimplexCalculateModule, &Simplex::tableCreated,
            [&](QString title, QVector<QVector<double>> &vectorData,
            QStringList rowsHeaders, QStringList columnHeaders){
        createTable(title, vectorData, rowsHeaders, columnHeaders);
    });

    connect(m_pTransportTaskModule, &TransportTask::sendText, this, &ModelManager::sendText);

    connect(m_pTransportTaskModule, &TransportTask::tableCreated,
            [&](QString title, QVector<QVector<double>> &vectorData,
            QStringList rowsHeaders, QStringList columnHeaders){
        createTable(title, vectorData, rowsHeaders, columnHeaders);
    });
}

ModelManager::~ModelManager()
{
    delete m_pTableModelCreator;
    delete m_pSimplexTableModel;
    delete m_pTransportTableModel;
    delete m_pSimplexCalculateModule;
    delete m_pTransportTaskModule;
}

void ModelManager::createTable(const QString title, const int rows, const int columns)
{
    emit tableModelCreated(title,
                           m_pTableModelCreator->createTableModel(rows, columns));
}

void ModelManager::createTable(const QString title, const QVector<QVector<double>> &vectorData)
{
    emit tableModelCreated(title,
                           m_pTableModelCreator->createTableModel(vectorData));
}

void ModelManager::createTable(const QString title, const QVector<QVector<double>> &vectorData,
                               QStringList rowsHeaders, QStringList columnHeaders)
{
    emit tableModelCreated(title,
                           m_pTableModelCreator->createTableModel(vectorData, rowsHeaders, columnHeaders));
}

void ModelManager::deleteTables()
{
    m_pTableModelCreator->deleteTables();
}

void ModelManager::initSimplexTableModel()
{
    QStringList rowHeaders, columnHeaders;

    //    QVector<QVector<double>> initialData(4);
    //    for (auto &rowVector : initialData) {
    //        rowVector.resize(4);
    //        rowVector.fill(0);
    //    }

    QVector<QVector<double>> initialData = {{19, 10, 11, 912},
                                            {24, 5, 13, 1539},
                                            {19, 10, 21, 798},
                                            {28, 27, 27, 0}
                                           };

    initialData.last().removeLast();

    rowHeaders << "Сырье 1" << "Сырье 2" << "Сырье 3" << "Прибыль";
    columnHeaders << "Изделие X" << "Изделие Y" << "Изделие Z" << "Запас сырья";

    m_pSimplexTableModel = m_pTableModelCreator->createTableModel(initialData, rowHeaders, columnHeaders);
}

void ModelManager::initTransportTableModel()
{
    QStringList rowHeaders, columnHeaders;

    //    QVector<QVector<double>> initialData(4);
    //    for (auto &rowVector : initialData) {
    //        rowVector.resize(4);
    //        rowVector.fill(0);
    //    }

    QVector<QVector<double>> initialData = {{   5,    5,    4,    5,    6, 12711},
                                            {   6,    7,    2,    6,    9, 16530},
                                            {   9,    5,    8,    3,    7, 9177},
                                            {3648, 9690, 6954, 7410, 5586, 0}
                                           };

    initialData.last().removeLast();

    rowHeaders << "A1" << "A2" << "A3" << "Потребности";
    columnHeaders << "B1" << "B2" << "B3" << "B4" << "B5" << "Запасы";

    m_pTransportTableModel = m_pTableModelCreator->createTableModel(initialData, rowHeaders, columnHeaders);

}

QObject *ModelManager::getSimplexTableModel()
{
    return m_pSimplexTableModel;
}

QObject *ModelManager::getTransportTableModel()
{
    return m_pTransportTableModel;
}

void ModelManager::resolveBySimplex()
{
    m_pSimplexCalculateModule->calculate(m_pSimplexTableModel->getData());
}

void ModelManager::resolveTransportTask()
{
    m_pTransportTaskModule->calculate(m_pTransportTableModel->getData());
}

TableModelCreator *ModelManager::getTableModelCreator()
{
    return m_pTableModelCreator;
}
