#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "init.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

    QGuiApplication app(argc, argv);

    Init initModule;

    initModule.init();

    int mainret =  app.exec();

    return mainret;
}
