#include "init.h"

Init::Init()
{
    m_pView = new QQuickView();
    m_pModelManager = new ModelManager();

    m_pView->rootContext()->setContextProperty("Init", this);
    m_pView->rootContext()->setContextProperty("ModelManager", m_pModelManager);
}

void Init::init()
{
    m_pModelManager->initSimplexTableModel();
    m_pModelManager->initTransportTableModel();

    m_pView->setSource(QUrl("qrc:/app/ui/main.qml"));

    m_pView->show();
}
