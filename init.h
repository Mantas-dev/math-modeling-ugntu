#ifndef INIT_H
#define INIT_H

#include <QQuickView>
#include <QQmlContext>

#include "app/modules/modelmanager.h"

class Init : public QObject
{
    Q_OBJECT
public:
    Init();

    void init();

signals:
    void requestPushPage(QString pageUrl);
    void requestPopPage();

private:
    QQuickView *m_pView;
    ModelManager *m_pModelManager;
};

#endif // INIT_H
